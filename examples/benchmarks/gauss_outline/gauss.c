#define N 3

#include <stdio.h>
#include <string.h>

typedef int DT;

typedef struct
{
	DT m[N*N]; // (1)
} InputMatrix;

typedef struct
{
	DT b[N]; // (1)
} InputVector;

typedef struct
{
	DT res[N];
} Output;

void identity(DT* OUTPUT_m) {
	for(int i = 0; i<N; i++) {
		for(int j = 0; j<N; j++) {
			if(i==j) {
				OUTPUT_m[i*N+j] = 1;
			 } else{
				OUTPUT_m[i*N+j] = 0; 
			 }	
		}
	}
}

/**
 * Recomputes the result once LU decomposition is completed
 */
void solve_backtracking(DT *m, DT *b, DT *OUTPUT_res) {
	for(int i = 0; i < N;i++) {
		OUTPUT_res[i] = 0;
	}

	OUTPUT_res[2] = b[N-1]/m[(N-1)*N+N-1];


	// // manually unroll loop:
	// int i = 1;
	// DT tmp = 0;
	// int j = i+1;
	// tmp += OUTPUT_res[j]*m[i*N+j];
	// OUTPUT_res[1] = (b[i] - tmp) / m[i*N+i];

	// // manually unroll loop:
	// i = 0;
	// tmp = 0;
	// j = i+1;
	// tmp += OUTPUT_res[j]*m[i*N+j];
	// j++;
	// tmp += OUTPUT_res[j]*m[i*N+j];
	// OUTPUT_res[0] = (b[i] - tmp) / m[i*N+i];

	// for (int i = 0; i < 2; i++) {
		// DT tmp = 0;
		// int x = 1-i;
		// for(int j = N-i; j < N; j++) {
			// tmp += OUTPUT_res[j]*m[x*N+j];
		// }
		// OUTPUT_res[x] = (b[x] - tmp) / m[x*N+x];
	// }

	// for(int i = N-2; i >=0; i--) {
	// 	DT tmp = 0;
	// 	for(int j = i+1; j < N; j++) {
	// 		tmp += OUTPUT_res[j]*m[i*N+j];
	// 	}
	// 	OUTPUT_res[i] = (b[i] - tmp) / m[i*N+i];
	// }
}

void swap(DT* m, DT* v, DT* OUTPUT_m, DT* OUTPUT_v, int n, int from, int to) {
	if(from!=to) {
		// Iterate over columns)
		for(int j = from; j < n; j++) {
			DT tmp = m[from*n+j];
			m[from*n+j] = m[to*n+j];
			m[to*n+j] = tmp;
		}
		DT tmp = v[from];
		v[from] = v[to];
		v[to] = tmp;
	}
	// memcpy(OUTPUT_m, m, N*N*sizeof(DT));
	for (int i = 0; i < N*N; i++) {
		OUTPUT_m[i] = m[i];
	}
	// memcpy(OUTPUT_v, v, N*sizeof(DT));
	for (int i = 0; i < N; i++) {
		OUTPUT_v[i] = v[i];
	}

}

/**
 * Performs the propagating swap for LU decomposition
 */
void pivot_swap(DT *m, DT *b, DT *OUTPUT_m, DT *OUTPUT_b, int i, int n) {
	// memcpy(OUTPUT_m, m, sizeof(DT)*N*N);
	for (int j = 0; j < N*N; j++) {
		OUTPUT_m[j] = m[j];
	}
	// memcpy(OUTPUT_b, b, sizeof(DT)*N);
	for (int j = 0; j < N; j++) {
		OUTPUT_b[j] = b[j];
	}

	for(int k=i+1; k < n; k++) {
		if(m[k*n+i] > m[i*n+i]) {
			swap(m, b, OUTPUT_m, OUTPUT_b, n, i, k);
			// memcpy(m, OUTPUT_m, sizeof(DT)*N*N);
			for (int j = 0; j < N*N; j++) {
				m[j] = OUTPUT_m[j];
			}
			// memcpy(b, OUTPUT_b, sizeof(DT)*N);
			for (int j = 0; j < N; j++) {
				b[j] = OUTPUT_b[j];
			}
		}
	}
	// memcpy(OUTPUT_m, m, sizeof(DT)*N*N);
	// memcpy(OUTPUT_b, b, sizeof(DT)*N);
}


/**
 *  With Pivoting and active swap
 */
void gaussj_E(DT *m, DT *b, DT *OUTPUT_res) {
	InputMatrix L;
	identity(L.m);
	for(int i= 0; i < N-1; i++) {
		DT m_tmp[N*N];
		DT b_tmp[N];
		pivot_swap(m, b, m_tmp, b_tmp, i, N);
		// memcpy(m, m_tmp, sizeof(DT)*N*N);
		for (int j = 0; j < N*N; j++) {
			m[j] = m_tmp[j];
		}
		// memcpy(b, b_tmp, sizeof(DT)*N);
		for (int j = 0; j < N; j++) {
			b[j] = b_tmp[j];
		}
		for(int k=i+1; k < N; k++) {
			L.m[k*N+i] = m[k*N+i] / m[i*N+i]; // Yao candidate?
			// Arithmetic candidate
			for(int j = i; j < N; j++) {
				m[k*N+j] = m[k*N+j] - L.m[k*N+i] * m[i*N+j];
				//a.m[k*N+j] = a.m[k*N+j] - fixedpt_mul(L.m[k*N+i],a.m[i*N+j]);
			}
			b[k] = b[k] - L.m[k*N+i] * b[i];
		}			
	}
	solve_backtracking(m, b, OUTPUT_res);
}

void mpc_main() {
	InputMatrix INPUT_A_m;
	InputVector INPUT_B_b;
	Output OUTPUT_res;
	gaussj_E(INPUT_A_m.m, INPUT_B_b.b, OUTPUT_res.res);
}

