#define N 256
#define K 4 // currently fixed, do not change

#define INNER 64
#define OUTER (N/INNER)

#include <inttypes.h>
#include <string.h>

typedef int32_t int_t;

int_t match_fix(int_t x1, int_t x2,int_t x3, int_t x4, int_t y1, int_t y2, int_t y3, int_t y4) {
  int_t r = 0;
  int i;
  int t1 = (x1-y1);
  int t2 = (x2-y2);
  int t3 = (x3-y3);
  int t4 = (x4-y4);
  r = t1*t1 + t2*t2 + t3*t3 + t4*t4;
  return r;
}

int_t min(int_t *data, int len) {
  int_t best_match = data[0];
  for(int i = 1; i < N; i++) {
    if(data[i] < best_match) {
      best_match = data[i];
    }
  }
  return best_match;
}

void mpc_main() {
  int_t INPUT_A_db[N][K];
  int_t INPUT_B_sample[K];
  int_t matches[N];

  // Compute distances
  for(int i = 0; i < N; i++) {
    matches[i] = match_fix(INPUT_A_db[i][0], INPUT_A_db[i][1], INPUT_A_db[i][2], INPUT_A_db[i][3], INPUT_B_sample[0], INPUT_B_sample[1], INPUT_B_sample[2], INPUT_B_sample[3]);
  }
  // Compute minimum
  int_t best_match = min(matches, N);
  int_t OUTPUT_res = best_match;
}
