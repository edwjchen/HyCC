#define N 256
#define K 4 // currently fixed, do not change

#define INNER 64
#define OUTER (N/INNER)

#include <inttypes.h>
#include <string.h>

typedef int32_t int_t;

void mpc_main() {
  int_t INPUT_A_db[N][K];
  int_t INPUT_B_sample[K];
  int_t matches[N];

  // Compute distances
  for(int i = 0; i < N; i++) {
    int t1 = (INPUT_A_db[i][0]-INPUT_B_sample[0]);
    int t2 = (INPUT_A_db[i][1]-INPUT_B_sample[1]);
    int t3 = (INPUT_A_db[i][2]-INPUT_B_sample[2]);
    int t4 = (INPUT_A_db[i][3]-INPUT_B_sample[3]);
    int res = t1*t1 + t2*t2 + t3*t3 + t4*t4; 
    matches[i] = res;
  }

  // // Compute distances
  // for(int i = 0; i < N; i++) {
  //   int t1 = (INPUT_A_db[i][0]-INPUT_B_sample[0]);
  //   int t2 = (INPUT_A_db[i][1]-INPUT_B_sample[1]);
  //   int t3 = (INPUT_A_db[i][2]-INPUT_B_sample[2]);
  //   int t4 = (INPUT_A_db[i][3]-INPUT_B_sample[3]);
  //   matches[i] = t1*t1 + t2*t2 + t3*t3 + t4*t4; 
  // }
  
  // Compute minimum
  int_t best_match = matches[0];
  for(int i = 1; i < N; i++) {
    if(matches[i] < best_match) {
      best_match = matches[i];
    }
  }
  int_t OUTPUT_res = best_match;
}
